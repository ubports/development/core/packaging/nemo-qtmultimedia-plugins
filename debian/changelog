nemo-qtmultimedia-plugins (0.1.1-2~ubports16.04.3) xenial; urgency=medium

  * Fix crash when QMediaService is null, fix Teleports animated sticker
    - 0007-don-t-crash-if-QMediaService-is-null.patch

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Mon, 19 Apr 2021 17:20:29 +0700

nemo-qtmultimedia-plugins (0.1.1-2~ubports16.04.2build1) xenial; urgency=medium

  * No-change rebuild after migrating to GitLab CI

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Sat, 09 Jan 2021 00:15:30 +0700

nemo-qtmultimedia-plugins (0.1.1-2~ubports16.04.2) xenial; urgency=medium

  * Bring back 0001-Link-to-multimediagsttools-for-qt-5.10.patch.
    We're transitioning to Qt 5.12. Thus, this is required.
  * debian/control: add a missing build-dependency

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Thu, 03 Sep 2020 22:41:21 +0700

nemo-qtmultimedia-plugins (0.1.1-2~ubports16.04.1) xenial; urgency=medium

  * Merge version 0.1.1-2 from Debian-PM to fix build failure on AMD64.

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Tue, 09 Jun 2020 02:09:46 +0700

nemo-qtmultimedia-plugins (0.1.1-2) unstable; urgency=medium

  * Bring one of dropped patches back to fix build on amd64
    - 0003-texturevideobuffer-Add-missing-include.patch

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Tue, 09 Jun 2020 02:03:39 +0700

nemo-qtmultimedia-plugins (0.1.1-1~ubports16.04.1) xenial; urgency=medium

  * Merge version 0.20200421.0-1 from Debian-PM
  * Update source location for 0.1.1
  * Remove applied upstream patch
    - 0004-make-sure-orientation-set-before-init-is-respected.patch
  * Update patches that don't apply with the newer version
    - 0005-make-sure-backends-nativeSize-isnt-affected-by-orientation-setting.patch
    - 0006-correct-the-meaning-of-adjustedViewport.patch

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Tue, 09 Jun 2020 00:49:55 +0700

nemo-qtmultimedia-plugins (0.1.1-1) unstable; urgency=medium

  * New upstream release. Changes since last update includes:
    - Make sure orientation is set before init() is applied.
    - videotexturebackend: call attached QML video filters.
    - Use persistent bindings of graphics buffers to texture ids.
  * d/control: add a version requirement to nemo-gst-interfaces.
    This is due to API changes required by the new version.
  * Remove patches applied upstream/not needed
    - 0001-Fix-Qt-Multimedia-attached-video-filters-in-QML.patch
    - 0002-Try-to-call-video-filters-without-holding-mutex.patch
    - 0003-texturevideobuffer-Add-missing-include.patch

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Fri, 08 May 2020 19:39:38 +0700

nemo-qtmultimedia-plugins (0.0.11-1~ubports16.04.2) xenial; urgency=medium

  * Add patches to fix camera focus point selection
    - debian/patches/0004-make-sure-orientation-set-before-init-is-
      respected.patch
    - debian/patches/0005-make-sure-backends-nativeSize-isnt-affected-by-
      orientation-setting.patch
    - debian/patches/0006-correct-the-meaning-of-adjustedViewport.patch

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Tue, 15 Oct 2019 20:53:14 +0700

nemo-qtmultimedia-plugins (0.0.11-1~ubports16.04.1+rebuild1) xenial; urgency=medium

  * No-change rebuild after build failure due to CI problem.

 -- Ratchanan Srirattanamet <peathot@hotmail.com>  Tue, 24 Sep 2019 22:22:13 +0700

nemo-qtmultimedia-plugins (0.0.11-1~ubports16.04.1) xenial; urgency=medium

  * Import to UBports
  * Drop debian/patches/0001-Link-to-multimediagsttools-for-qt-5.10.patch.
    UBports 16.04 still uses Qt 5.9. So, this is no use to us.

 -- Ratchanan Srirattanamet <peathot@hotmail.com>  Wed, 18 Sep 2019 23:58:41 +0700

nemo-qtmultimedia-plugins (0.0.11-1) unstable; urgency=medium

  * Initial release.

 -- Jonah Brüchert <jbb@kaidan.im>  Fri, 19 Jul 2019 19:23:33 +0200
